from requests import get
from bs4 import BeautifulSoup
from time import sleep
from random import randint
from time import time
from IPython.display import clear_output
 
import pandas as pd
 
 

starts = [str(i) for i in range(1,201,50)]
 
years_url = [str(i) for i in range(2000,2018)]
 
# déclaration des listes 
names = []
years = []
imdb_ratings =[]
metascores = []
votes = []
 
# initialisation des boucles
start_time = time()
requests = 0
 
# On balaie pour chaque année de l'intervalle 2000-2017
for year_url in years_url:
    
    # on boucle ensuite  sur les starts contenant les premiers films des pages de 1 à 4
    for start in starts:
        
        
        response = get('http://www.imdb.com/search/title?release_date='+ year_url +'&sort=num_votes,desc&start='+ start)
        
        # 6. Pause de la boucle pour éviter de se faire kicked par le serveur
        sleep(randint(8,15))
        
        # affichage info requete
        requests += 1
        elapsed_time = time() - start_time
        print('Request: {}; Frequency: {} requests/s'.format(requests, requests/elapsed_time))
        clear_output(wait = True)
        
        # Warning si != de 200
        if response.status_code != 200:
            warn('Request: {}; Status code: {}'.format(requests, response.status_code))
            
        # Stopper la boucle si le nombre de requêtes est supérieur à 72
        if requests > 72:
            warn('Nombre de requêtes trop important')
            break
            
        # Extraire le HTML avec BeautifulSoup
        page_html = BeautifulSoup(response.text, 'html.parser')
        
        # On prend  les 50 films de chaque page (container)
        mv_containers = page_html.find_all('div', class_="lister-item mode-advanced")
        
        # on Boucle pour chaque container
        for container in mv_containers:
            # Si le film a un Metascore, on extrait
            if container.find('div', class_='ratings-metascore') is not None:
                
               
                name = container.h3.a.text
                names.append(name)
                
                
                year = container.h3.find('span', class_='lister-item-year').text
                years.append(year)
                
                
                imdb = float(container.strong.text)
                imdb_ratings.append(imdb)
                
                
                metascore = container.find('span', class_='metascore').text
                metascores.append(int(metascore))
                
                
                vote = container.find('span', attrs = {'name':'nv'})['data-value']
                votes.append(int(vote))
 
#Afficher les 100 premiers films dans un DataFrame
movie_ratings = pd.DataFrame({
        'movie': names,
        'year': years,
        'imdb': imdb_ratings,
        'metascore': metascores,
        'votes': votes
    })
 
movie_ratings.head(100)
